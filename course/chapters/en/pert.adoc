== Planning

The main role of a project manager is to organize the use of the different resources at his disposal
(workload of the teams, ...) by dividing them into different activities, or tasks,
whose sequencing ensures that the project's perimeter is respected.

A task is a set of actions that, when fully completed, mark significant progress on the project.
The project itself is completed when all the tasks that make it up are completed.

The scope of each task must be correctly defined:

* if a task is not detailed enough, visibility will not be good and project management will not be facilitated.
* if a task is too detailed, too many tasks will in turn obstruct visibility.

The PERT/CPM diagram and the Gantt chart are two widely used techniques for planning.
These two types of diagrams make it possible to coordinate more or less sequential tasks.
with the aim of obtaining a realistic forecast of times and costs.

=== PERT/CPM

PERT/CPM allows to highlight :

* the dependencies between tasks
* parallelisation of tasks
* three estimates (optimistic, pessimistic, realistic) of the duration of tasks
* the margin of the tasks and the project they make up

==== Characteristics of a task
Each task `T` offers the following features :

* *Duration* : The `D(T)` time required to complete the task.
* *Predecessors*: Other tasks which must be completed before `T` can begin.
* *Successors*: Other tasks having `T` as a predecessor.
* *ES*: Earliest Start Time `ES(T)`.
  `T` cannot start before this date.
* *EF*: Earliest Finish Time `EF(T)`.
  `T` cannot end before this date.
* *LS*: Latest Start Time `LS(T)`.
  `T` must begin before this date.
* *LF*: Latest Finish Time `LF(T)`.
  `T` must be completed before this date.
* *Slack*: The Slack `S(T)` is the maximum allowable delay (_Float_).

image::pert/task.png[caption="Figure 01:", title="Task PERT/CPM", alt="Example of representation"]

`EF(T) = ES(T) + D(T)`: a task cannot finish before it is fully completed. +
`LS(T) = LF(T) - D(T)`: a task must be fully completed before finishing. +
`ES(T) = max(FO(predecessors(T))`: a task can only start when its prerequisites are finished. +
`LF(T) = min(DA(successors(T))`: the successors of a task can only start when the task is finished. +
`S(T) = LF(T) - EF(T)`: the slack is equal to the delay between the date on which it _must_ finish and the date on which it _can_ finish.

image::pert/chart.png[caption="Figure 02:", title="PERT/CPM diagram", alt="PERT/CPM"]

==== Critical path

A task with a slack of 0 is considered *critical*.
This means that the slightest delay in the completion of this task will delay the entire project by the same amount.

The *critical path* is the longest path in the PERT diagram.
All tasks in the critical path have the same slack (possibly 0, but not necessarily).
If a task on the critical path is delayed by more than its slack, the whole project will be delayed by the same amount.

===== Milestones

It may be relevant to create *milestones* in a PERT diagram.
A milestone is typically a "virtual" task whose duration is equal to 0.
It marks (and allows to control) an important moment in the progress of a project.

===== Topology

* Is there exactly one beginning task and one end task ?
* Do all tasks have at least one predecessor and one successor?
* Are all links representative?

image::pert/error-1.png[caption="Figure 03.1:", title="Redundant transition", alt="Redundancy"]

image::pert/error-2.png[caption="Figure 03.2:", title="Creating a milestone", alt="Simplification"]

==== Step by step

1. *Identify* _all_ the tasks to be carried out, without neglecting certain "ancillary" tasks :
** Does the project require technologies that are not mastered internally (training needs, purchasing procedure, ...)?
** Is the workload too heavy for the current teams (recruitment needs, ...)?
** Is everything present to ensure the quality of the project (need for documentation, tests, ...) ?
** Does a particularly complex task need to be managed as a sub-project ?
2. *Estimate the duration* of each task.
   Since it is impossible to know the actual duration of each task during the planning phase, it is estimated as a variable, probabilistically.
** Mist likely time `TM`: expected/realistic/average working time required under "normal" conditions
** Optimistic time `TO`: working time under "ideal" conditions (no interruptions or unforeseen difficulties, ...)
** Pessimistic time `TP`: working time in the "worst" conditions (shortage of staff, multiple unforeseen difficulties, ...)
** Duration is therefore an "expected" duration, obtained using the following formula:
  `D = (TO + 4×TM + TP)÷6`
** It is when calculating the duration that one chooses (or not) to grant oneself a precautionary period.
   Afterwards, it is too late; in particular, the subsequent calculation of the slack is not there for that!
3. *Scheduling* tasks
** organize their chronological sequencing
** optimize by considering possible parallelisms
** determine the critical path
4. *Planning* the different tasks
** allocation of the necessary human resources
** allocation of the necessary material resources
** more (or better) resources could be allocated on critical tasks

=== Gantt

Compared to a PERT/CPM diagram, a Gantt chart is perhaps simpler to understand.
It has the advantage of clearly showing the start and end dates,
the percentage of completion of each task, as well as any overlap between tasks.
On the other hand, it places less emphasis on the dependencies between tasks and does not take into account sleck or risk.

In a Gantt chart, a task has only a start or end date and a duration.
Tasks are then linked in a logical sequence, using one of four types of relationships :

* from Finish to Start (FS): a task cannot start before the previous one has ended.
* from Start to Start (SS): a task cannot start before the previous one starts.
* from Finish to Finish (FF): a task cannot end before the previous one is ends.
* from Start to Finish (SF): a task cannot end before the previous one starts.

A task can of course have several other tasks preceding it.
Similarly, a task can precede several other tasks.

image::pert/gantt.png[caption="Figure 04:", title="Example of a Gantt chart: Writing a book", alt="Gantt"]
