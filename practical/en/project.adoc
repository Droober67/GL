= Project
Régis WITZ <rwitz@unistra.fr>
:doctype: book
:toc:
:toc-title:
:toclevels: 2



== Introduction

This project is intended to put into practice the knowledge and techniques acquired in the courses and practical work.
However, it may also allow you to experiment with new topics, techniques and tools, if you feel these are more appropriate to the task at hand, more interesting for the career you are considering, or simply more fun.



[[description]]
== Subject

This project aims to propose an implementation of *Love Letter*, a Seiji Kanai card game for 2 to 4 players.
The rules of the game are described link:http://online.fliphtml5.com/mvgr/hyvg/[in this PDF].
This document serves as a specification for this project.

You can also discover them in link:https://www.youtube.com/watch?v=WAiI7G3QdOU[this video].



== Groups

This project will be carried out in groups of *5 students*.
Each student can only be a member of one group.

Try to ensure that your group is functional, that everyone can do their share of the work, in a way that suits their desires and abilities.
As you will notice when reading this topic, an important part of your success lies in your analytical, conceptual and writing skills.
Therefore, do not hesitate to play on everyone's strengths.
For example, if you have someone in your group who is less good at coding but more at project tracking and reporting, it may be helpful to have that person rigorously take on the task of writing reports and keeping track of other team members, even if it means not writing a single line of code.
The main thing is to maximize the quality of your project as a whole (code _and_ reports), but also to get as much out of it as possible for your future career.

Solo students will not be accepted, for two reasons:

* Schedule constraints for support and correction.
* Knowing how to convince, communicate, share responsibilities, organize common work sessions in short, working in groups, is a vital set of professional skills that you need to develop.



[[modules]]
== Implementation

The implementation of the project requires two mandatory modules: <<module_core,core software>> and <<module_gui,GUI>>.
In order to allow you to explore certain areas at your convenience, and to optimize your score accordingly, this topic also offers three optional modules: <<module_ai,AI>>, <<module_network,network>> and <<module_plugins,extensions>>.

The technologies used are left free.
However, you will have to take into account the following constraints:

* The whole group must agree on the appropriateness of the chosen technologies.
  Your project can of course use several different technologies, but they must communicate correctly, and be appropriate to their respective roles.
  Your different choices will have to be motivated in your report.
* Your project must be playable from any machine equipped with a Firefox (version 72 or higher) or Chromium (version 79 or higher) browser.



[[module_core]]
=== Software Core (Module 0)

The project will be based on a software kernel in charge of the rules of the game.

* The rules of the game will have to be implemented in strict compliance with the specification given in the statement.
  You can propose new rules or modify existing rules, but this must be done via one or more <<module_plugins,extensions>>.
  The base rules must always remain accessible.
* The implementation of the kernel should be independent and offer clear interfaces to other modules.
  In other words, your implementation should respect the principle of https://en.wikipedia.org/wiki/Separation_of_concerns[separation of concerns]: for example, no notion of UI, AI or network should permeate the design of the kernel.

As you'll notice from your research, the creators and the Love Letter gaming community have created many additional cards that can be integrated into the game.
The following base cards are the only ones that must be implemented:

* _Guard_: value 1, 5 copies, loses if guessing
* _Priest_: value 2, 2 copies, look at the card
* _Baron_: value 3, 2 copies, compare cards
* _Handmaid_: value 4, 2 copies, protects
* _Prince_: value: 5, 2 copies, makes discard
* _King_: value 6, 1 copy, exchange cards
* _Countess_: value 7, 1 copy, to be discarded if King or Prince
* _Princess_: value 8, 1 copy, lost if discarded

You can implement others, but it will have to be done in a different way, via one or more <<module_plugins,extensions>>.



[[module_gui]]
=== Graphical User Interface (Module 1)

This project is really an _adaptation_ of the rules of Love Letter into a video game.
As you know, the experience of playing a game with paper cards around a gaming table is quite different from a video game.
Each medium has its strengths and weaknesses.
Your goal for this project is to make the most of the strengths of the computer medium, to sublimate the experience of Love Letter through the video game.
You should not seek to imitate at all costs the original experience around a table, such as the one shown in the videos given as an illustration in the <<description,statement>>.

In the same way, you are absolutely free to define the details and the _look and feel_ of your GUI.
However, keep in mind the following points:

* The final result will have to demonstrate as much as possible the qualities of a good Man-Machine Interface as discussed in the course, especially usability.
* Your interface will have to be localized for at least two countries speaking different languages.
* The profile of the majority of you is certainly not that of an artist or a graphic designer.
  However, this should not prevent you from trying to find solutions that are technically within your reach and maximize the aesthetic appeal of your interface.
  As far as the exterior aesthetics are concerned, you are absolutely not obliged to use the _look & feel_ of the original.
  However, remember that appearance has an influence on usability: the result should form a coherent "whole".

If you choose to implement neither <<module_ai,artificial intelligence>> nor <<module_network,networking>>, you will have to propose a solution to allow several humans to play locally, on the same screen.



[[module_ai]]
=== Artificial Intelligence (Module 2, optional)

This module is optional.

Love Letter is played by default by 2 to 4 players.
If you want to allow a single player to play your application, you need to provide at least one Artificial Intelligence that will take the place of the missing player(s).

This AI can be very silly, or on the contrary always play optimally.
For a better gaming experience for the human player, it will also be able to adapt to his style of play, in order to offer him an interesting challenge.

Optionally, it might be possible to choose from several AIs with different playing styles.



[[module_network]]
=== Network (Module 3, optional)

This module is optional.

Love Letter is played by default by 2 to 4 players.
You can choose to allow several users to launch your application each on their own machine, and join to play together, automatically or via chat rooms or a matchmaking system, for example.

The format of the data exchanged is left free.
However, the chosen protocol should support all the other features of your application.



[[module_plugins]]
=== Plugins (Module 4, optional)

This module is optional.

As you will notice while doing your research, Love Letter is available in many more or less official versions.
Each version "reskins" the cards, altering their name, illustration and, more rarely, their power.
You can choose to allow this, and even go further, by allowing _mod_ developpers to invent new rules or new game modes.

This module consists in designing the software kernel so that it can accept a number of extensions (or _plugins_).
The nature of the extensions is not defined in advance: a well-designed extension engine will expose as many features as possible, so that third-party developers can modify them or add new ones.

Extensions are not part of the software code.
Instead, they are loaded at launch or dynamically while the application is running.
On the other hand, any extension must obviously respect a clearly defined formalism when designing the application.

In order to demonstrate the efficiency of your engine, you will implement one or several extensions by explaining their purpose: new game modes, or new maps (for example the link:https://tesera.ru/images/items/831604/1600x1200xsxy/photo.jpg[new characters of the premium edition]).

Optionally, you can also demonstrate this efficiency even more convincingly by having your application load one or more extensions developed by another group.
You will specify the other group in your report, and this will obviously not be considered plagiarism.
However, you will have to agree with the other group on the interfaces and data structures used.
You'll also need to agree on a method for publishing extensions, via a Gitlab repository, for example. +
Note that this method will be worth extra points _to both groups_: the group that developed the extension, as well as the group that supported the extension.
The exact point count for each group will depend on the work done (if two groups design one or more extensions together, no problem, they will be rewarded in the same way).

Your report should clearly state the advantages and limitations of your extension engine.



[[reports]]
== Report(s)

The various reports to be submitted must, among other things, detail and justify the ergonomic and technical choices you have made.
Their number (one document, several, ...), their formalism (descriptive, _story-driven_, ...) and their layout are free.
Their format is also free : _HTML_, _.pdf_, _.odt_, _LaTeX_, ... your only constraint is that I can read it.
In particular, I don't have a license Microsoft™©® Word™©®, so avoid the _.docx_.

Some suggestions about informations to publish in your report:

* the software interfaces between the different <<modules,modules>> that your group has chosen to implement: services/functions called, format of the data exchanged, and so on
* how you have organized yourself as a development team: division of labour, planning and milestones, and so on
* what you got out of the project: planning, successes, difficulties, lessons learned, experience gained, ... And this, both on a technical and human level.



[[source]]
== Software delivery

Each group will have to create a Gitlab repository containing the *source code* of their project.
This source code should:

* include all that is necessary so anyone can build it (if necessary).
  This information will have to be explained in a README, and at best be executed by a build script.
* demonstrate software design qualities such as modularity and good maintainability.

The repository can contain any element allowing you to demonstrate appropriate software engineering practices.

Right from the start of the project, each group must invite their teachers so they can check how the project progresses.


[[presentation]]
== Presentation

Each group will *present* its work.

The presentation may retell the content of the report(s) and/or develop certain points of interest.
These interviews will last about twenty minutes, and each group will have to be present in full.

Here again, try to see your presentation as an opportunity to progress, to express yourself in public, to develop your synthesis skills, among others!



[[grades]]
== Grades

Your software must be working, show as many qualities as possible, follow as many good practices of software engineering.

In addition to these basic requirements, there are a whole host of areas in which you can earn points to get a good score.
Here is a non-exhaustive list:

* Implementing one or more <<modules,optional modules>>.
  See their respective descriptions for more details.
* Make a particularly ergonomic software and motivate your choices in the defense and the report.
* Make a software accessible to the visually impaired and/or blind.
* Localize your GUI for a "r2l" culture (for example Arabic).
* Propose several game modes (solo, cooperative, competitive, local or network multiplayer).
* Make your application run on mobile (specify the target systems in your report, and demonstrate this portability during the presentation).
* Protect your application by automated (unit/functional) tests, use a continuous integration server.
* Take care of your delivery.
* Write one or more particularly detailed and quality reports (design diagrams, proof of your analysis work, comparison with the state of the art, study of alternatives, implementation difficulties, solutions considered but not retained and why, etc).
* Propose a convincing user manual, either in "paper" format (_ie._ PDF) or integrated into your application (online manual or tutorial).
* Rock your presentation!

Of course, any group caught plagiarizing someone else's work without giving it the credit it deserves will be subject to the usual sanctions, applied with the utmost severity.
